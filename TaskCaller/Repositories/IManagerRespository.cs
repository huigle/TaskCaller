﻿using System;
namespace TaskCaller.Repositories
{
    public interface IManagerRespository
    {
        bool Login(string username, string password);
    }
}
