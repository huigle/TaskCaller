﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using TaskCaller.Models.entity;
using TaskCaller.Services;
using TaskCaller.UI;

namespace SchedulerLite.Manager.Controllers
{
    public class DelayTaskController : Controller
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (TaskCaller.Startup.IsLogin(context.HttpContext))
            {
                base.OnActionExecuting(context);
            }
            else
            {
                context.Result = Redirect("/login");
            }
        }

        DelayTaskService _delayTaskService;
        public DelayTaskController(DelayTaskService delayTaskService)
        {
            _delayTaskService = delayTaskService;
        }

        // GET: DelayTask
        public ActionResult List(string name, int page = 1)
        {
            var pageSize = 10;
            var plist = _delayTaskService.SearchList(name, page, pageSize);
            ViewBag.plist = plist.ToStaticPagedList();
            return View();
        }

        public ActionResult Edit(long id = 0)
        {
            var m = _delayTaskService.SingleById(id);
            if (m == null)
            {
                m = new DelayTask();
                m.Enable = true;
                m.Method = "GET";
                m.TimeoutSeconds = 15;
                m.MaxRetryCount = 3;
                m.RetrySeconds = 30;
            }
            ViewBag.m = m;
            return View();
        }
        public ActionResult DoEdit(DelayTask m)
        {
            var ro = _delayTaskService.Edit(m);
            return Json(ro);
        }

        public ActionResult DoDelete(long id)
        {
            var ro = _delayTaskService.Delete(id);
            return Json(ro);
        }

        public ActionResult NewTask(DelayTask m)
        {
            var ro = _delayTaskService.Edit(m);
            return Json(ro);
        }
    }
}